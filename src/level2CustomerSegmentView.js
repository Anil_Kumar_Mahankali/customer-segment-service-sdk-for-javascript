import CustomerSegmentView from './customerSegmentView';
import Level1CustomerSegmentView from './level1CustomerSegmentView';

/**
 * @class {Level2CustomerSegmentView}
 */
export default class Level2CustomerSegmentView extends CustomerSegmentView {

    _level1CustomerSegment:Level1CustomerSegmentView;

    /**
     * @param {number} id
     * @param {string} name
     * @param {Level1CustomerSegmentView} level1CustomerSegment
     */
    constructor(id:number,
                name:string,
                level1CustomerSegment:Level1CustomerSegmentView) {

        super(id, name);

        if (!level1CustomerSegment) {
            throw TypeError('level1CustomerSegment required');
        }
        this._level1CustomerSegment = level1CustomerSegment;

    }

    /**
     *
     * @returns {Level1CustomerSegmentView}
     */
    get level1CustomerSegment():Level1CustomerSegmentView {

        return this._level1CustomerSegment;

    }

}
