import CustomerSegmentView from './customerSegmentView';
import Level2CustomerSegmentView from './level2CustomerSegmentView';

/**
 * @class {Level3CustomerSegmentView}
 */
export default class Level3CustomerSegmentView extends CustomerSegmentView {

    _level2CustomerSegment:Level2CustomerSegmentView;

    /**
     * @param {number} id
     * @param {string} name
     * @param {Level2CustomerSegmentView} level2CustomerSegment
     */
    constructor(id:number,
                name:string,
                level2CustomerSegment:Level2CustomerSegmentView) {

        super(id, name);

        if (!level2CustomerSegment) {
            throw new TypeError('level2CustomerSegment required');
        }
        this._level2CustomerSegment = level2CustomerSegment;

    }

    /**
     *
     * @returns {Level2CustomerSegmentView}
     */
    get level2CustomerSegment():Level2CustomerSegmentView {

        return this._level2CustomerSegment;

    }

}
