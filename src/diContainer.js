import {Container} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import CustomerSegmentServiceSdkConfig from './customerSegmentServiceSdkConfig';
import GetCustomerSegmentWithIdFeature from './getCustomerSegmentWithIdFeature';
import ListLevel1CustomerSegmentsFeature from './listLevel1CustomerSegmentsFeature';
import ListLevel2CustomerSegmentsWithTypeFeature from './listLevel2CustomerSegmentsWithTypeFeature';
import ListLevel3CustomerSegmentsWithSubTypeFeature from './listLevel3CustomerSegmentsWithSubTypeFeature';

/**
 * @class {DiContainer}
 */
export default class DiContainer {

    _container:Container;

    /**
     * @param {CustomerSegmentServiceSdkConfig} config
     */
    constructor(config:CustomerSegmentServiceSdkConfig) {

        if (!config) {
            throw 'config required';
        }

        this._container = new Container();

        this._container.registerInstance(CustomerSegmentServiceSdkConfig, config);
        this._container.autoRegister(HttpClient);

        this._registerFeatures();

    }

    /**
     * Resolves a single instance based on the provided key.
     * @param key The key that identifies the object to resolve.
     * @return Returns the resolved instance.
     */
    get(key:any):any {
        return this._container.get(key);
    }

    _registerFeatures() {

        this._container.autoRegister(GetCustomerSegmentWithIdFeature);
        this._container.autoRegister(ListLevel1CustomerSegmentsFeature);
        this._container.autoRegister(ListLevel2CustomerSegmentsWithTypeFeature);
        this._container.autoRegister(ListLevel3CustomerSegmentsWithSubTypeFeature);

    }

}
