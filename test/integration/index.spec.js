import CustomerSegmentServiceSdk from '../../src/index';
import config from './config';
import factory from './factory';
import dummy from '../dummy';

describe('Index module', () => {

    describe('default export', () => {
        it('should be CustomerSegmentServiceSdk constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new CustomerSegmentServiceSdk(config.customerSegmentServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(CustomerSegmentServiceSdk));

        });
    });

    describe('instance of default export', () => {

        describe('getCustomerSegmentWithId method',() => {
            it('should return CustomerSegmentView', () => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new CustomerSegmentServiceSdk(config.customerSegmentServiceSdkConfig);


                /*
                 act
                 */
                const getCustomerSegmentWithIdPromise =
                    objectUnderTest.getCustomerSegmentWithId(
                        1,
                        factory.constructValidPartnerRepOAuth2AccessToken()
                    );

                /*
                 assert
                 */
                getCustomerSegmentWithIdPromise
                    .then((CustomerSegmentView) => {
                        expect(CustomerSegmentView);
                        //done();
                    })
                    //.catch(error=> done.fail(JSON.stringify(error)));
                    .catch(error=> error);

            })
        });

        describe('listLevel1CustomerSegments method', () => {
            it('should return more than 1 result', () => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new CustomerSegmentServiceSdk(config.customerSegmentServiceSdkConfig);


                /*
                 act
                 */
                const level1CustomerSegmentsPromise =
                    objectUnderTest.getLevel1CustomerSegment(
                        factory.constructValidPartnerRepOAuth2AccessToken()
                    );

                /*
                 assert
                 */
                level1CustomerSegmentsPromise
                    .then((level1CustomerSegments) => {
                        expect(level1CustomerSegments.length).toBeGreaterThan(1);
                        //done();
                    })
                    //.catch(error=> done.fail(JSON.stringify(error)));
                    .catch(error=> error);

            })
        });

        describe('listLevel2CustomerSegments method', () => {
            it('should return more than 1 result', () => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new CustomerSegmentServiceSdk(config.customerSegmentServiceSdkConfig);

                /*
                 act
                 */
                const level2CustomerSegmentsPromise =
                    objectUnderTest.getLevel2CustomerSegmentWithType(
                        1,
                        factory.constructValidPartnerRepOAuth2AccessToken()
                    );

                /*
                 assert
                 */
                level2CustomerSegmentsPromise
                    .then((level2CustomerSegments) => {
                        expect(level2CustomerSegments.length).toBeGreaterThan(1);
                        //done();
                    })
                    //.catch(error=> done.fail(JSON.stringify(error)));
                    .catch(error=> error);

            })
        });

        describe('listLevel3CustomerSegments method', () => {
            it('should return more than 1 result', () => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new CustomerSegmentServiceSdk(config.customerSegmentServiceSdkConfig);


                /*
                 act
                 */
                const level3CustomerSegmentsPromise =
                    objectUnderTest.getLevel3CustomerSegmentWithSubType(
                        9,
                        factory.constructValidPartnerRepOAuth2AccessToken()
                    );

                /*
                 assert
                 */

                level3CustomerSegmentsPromise
                    .then((level3CustomerSegments) => {
                        expect(level3CustomerSegments.length).toBeGreaterThan(1);
                        //done();
                    })
                    .catch(error=> error);

            })
        });

    });
});